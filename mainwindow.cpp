#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QColorDialog>
#include <QFileDialog>
#include <QMessageBox>
#include <QDesktopServices>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->label_showDiagramColor->setAutoFillBackground(true);
    ui->label_showGridColor->setAutoFillBackground(true);
    ui->label_showPeakColor->setAutoFillBackground(true);
    ui->label_showPointColor->setAutoFillBackground(true);
    ui->label_showDiagramCoordinateColor->setAutoFillBackground(true);

    /*DIAL COMPONENT*/
    connect(ui->dial_diagramLevel,SIGNAL(valueChanged(int)),this,SLOT(slot_ComboBox_SetDiagramLevel(int)));

    /*COMBO BOX*/
    connect(ui->comboBox_Grid,SIGNAL(currentIndexChanged(int)),this,SLOT(slot_ComboBox_GridBackground(int)));
    connect(ui->comboBox_Accuracy,SIGNAL(currentIndexChanged(int)),this,SLOT(slot_ComboBox_SetDiagramAccuracy(int)));
    connect(ui->comboBox_LineWidth,SIGNAL(currentIndexChanged(int)),this,SLOT(slot_ComboBox_SetLineWidth(int)));
    connect(ui->comboBox_pointShape,SIGNAL(currentIndexChanged(int)),this,SLOT(slot_ComboBox_SetPointShape(int)));
    connect(ui->comboBox_peaksShape,SIGNAL(currentIndexChanged(int)),this,SLOT(slot_ComboBox_SetPeakShape(int)));

    /*PUSH BUTTON*/
    connect(ui->pushButton_colorChooser_DiagramCoordinate,SIGNAL(clicked(bool)),this,SLOT(slot_ColorChooser_CoordinateDiagram()));
    connect(ui->pushButton_colorChooser_Diagram,SIGNAL(clicked(bool)),this,SLOT(slot_ColorChooser_Diagram()));
    connect(ui->pushButton_colorChooser_Grid,SIGNAL(clicked(bool)),this,SLOT(slot_ColorChooser_GridLines()));
    connect(ui->pushButton_colorChooser_NavPoint,SIGNAL(clicked(bool)),this,SLOT(slot_ColorChooser_NavPoint()));
    connect(ui->pushButton_colorChooser_PeakPoint,SIGNAL(clicked(bool)),this,SLOT(slot_ColorChooser_PeaksShape()));

    connect(ui->pushButton_Restore,SIGNAL(clicked(bool)),this,SLOT(slot_Button_Restore()));
    connect(ui->pushButton_saveDiagram,SIGNAL(clicked(bool)),this,SLOT(slot_Button_Save()));
    connect(ui->pushButton_about,SIGNAL(clicked(bool)),this,SLOT(slot_Button_About()));
    connect(ui->pushButton_help,SIGNAL(clicked(bool)),this,SLOT(slot_Button_Help()));
    connect(ui->pushButton_close,SIGNAL(clicked(bool)),this,SLOT(close()));

    /*CHECK BOX*/
    connect(ui->checkBox_diagramCoordinate,SIGNAL(toggled(bool)),this,SLOT(slot_CheckBox_SetDiagramCoordinate(bool)));
    connect(ui->checkBox_diagramNavigator,SIGNAL(toggled(bool)),this,SLOT(slot_CheckBox_Navigation(bool)));
    connect(ui->checkBox_showPeaks,SIGNAL(toggled(bool)),this,SLOT(slot_CheckBox_ShowPeaks(bool)));
    connect(ui->checkBox_showPeaksText,SIGNAL(toggled(bool)),this,SLOT(slot_CheckBox_PeaksValues(bool)));


    ui->plotter_widget->setMouseTracking(true);
    /*set the first settings*/
    ui->plotter_widget->setGridBGSquareNo(ui->comboBox_Grid->currentIndex());
    ui->plotter_widget->setDimention(ui->plotter_widget->width(),ui->plotter_widget->height());


    QString values;
    m_color_coordinateLines = QColor(Qt::lightGray);
    values=QString("%1,%2,%3").arg(QString::number(m_color_coordinateLines.red()),
                                           QString::number(m_color_coordinateLines.green()),
                                           QString::number(m_color_coordinateLines.blue())
                                           );
    ui->label_showDiagramCoordinateColor->setStyleSheet("background-color: rgb("+values+");");
    m_color_coordinateLines = QColor(Qt::black);/*Default Color*/


    m_color_navPoint = QColor(Qt::lightGray);
    values=QString("%1,%2,%3").arg(QString::number(m_color_navPoint.red()),
                                           QString::number(m_color_navPoint.green()),
                                           QString::number(m_color_navPoint.blue())
                                           );
    ui->label_showPointColor->setStyleSheet("background-color: rgb("+values+");");
    m_color_navPoint = QColor(Qt::blue);/*Default Color*/


    m_color_peaks = QColor(Qt::lightGray);
    values=QString("%1,%2,%3").arg(QString::number(m_color_peaks.red()),
                                           QString::number(m_color_peaks.green()),
                                           QString::number(m_color_peaks.blue())
                                           );
    ui->label_showPeakColor->setStyleSheet("background-color: rgb("+values+");");
    m_color_peaks = QColor(Qt::darkRed);/*Default Color*/

    m_color_diagram = Qt::red;
    m_color_grid = Qt::darkGray;
    values=QString("%1,%2,%3").arg(QString::number(m_color_grid.red()),
                                           QString::number(m_color_grid.green()),
                                           QString::number(m_color_grid.blue())
                                           );
    ui->label_showGridColor->setStyleSheet("background-color: rgb("+values+");");

//    setWindowFlags(Qt::Dialog | Qt::MSWindowsFixedSizeDialogHint);

}

MainWindow::~MainWindow()
{
    delete ui;
}



void MainWindow::slot_ComboBox_GridBackground(int index)
{
    switch (index) {
    case EGRID_STATUS_NONE:
        ui->plotter_widget->setGridBGSquareNo(0);
        break;
    case EGRID_STATUS_SET_1:
        ui->plotter_widget->setGridBGSquareNo(2);
        break;
    case EGRID_STATUS_SET_2:
        ui->plotter_widget->setGridBGSquareNo(3);
        break;
    case EGRID_STATUS_SET_3:
        ui->plotter_widget->setGridBGSquareNo(5);
        break;
    case EGRID_STATUS_SET_4:
        ui->plotter_widget->setGridBGSquareNo(6);
        break;
    default:
        ui->plotter_widget->setGridBGSquareNo(8);
        break;
    }
    ui->plotter_widget->update();
}

void MainWindow::slot_ComboBox_SetLineWidth(int index)
{
    switch (index) {
    case 0:
        ui->plotter_widget->setLineWidth(0.25F);
        break;
    case 1:
        ui->plotter_widget->setLineWidth(0.5F);
        break;
    case 2:
        ui->plotter_widget->setLineWidth(1.0F);
        break;
    case 3:
        ui->plotter_widget->setLineWidth(1.5F);
        break;
    case 4:
        ui->plotter_widget->setLineWidth(2.0F);
        break;
    default:
        ui->plotter_widget->setLineWidth(1.0F);
        break;
    }
    ui->plotter_widget->update();
}

void MainWindow::slot_ComboBox_SetDiagramLevel(int index)
{
    ui->label_diagramLevel->setText(QString::number(index));
    ui->plotter_widget->setDiagramLevel(index);
    ui->plotter_widget->update();
}

void MainWindow::slot_ComboBox_SetPointShape(int index)
{
    switch (index) {
    case 0:
        ui->plotter_widget->setPointShape(0);
        break;
    case 1:
        ui->plotter_widget->setPointShape(1);
        break;
    case 2:
        ui->plotter_widget->setPointShape(2);
        break;
    default:
        ui->plotter_widget->setPointShape(0);
        break;
    }
    ui->plotter_widget->update();
}

void MainWindow::slot_ComboBox_SetPeakShape(int index)
{
    switch (index) {
    case 0:
        ui->plotter_widget->setPeakShape(0);
        break;
    case 1:
        ui->plotter_widget->setPeakShape(1);
        break;
    case 2:
        ui->plotter_widget->setPeakShape(2);
        break;
    default:
        ui->plotter_widget->setPeakShape(0);
        break;
    }
    ui->plotter_widget->update();
}


void MainWindow::slot_ComboBox_SetDiagramAccuracy(int index)
{
    switch (index) {
    case 0:
        ui->plotter_widget->setDiagramAccuracy(1);
        break;
    case 1:
        ui->plotter_widget->setDiagramAccuracy(4);
        break;
    case 2:
        ui->plotter_widget->setDiagramAccuracy(8);
        break;
    default:
        ui->plotter_widget->setDiagramAccuracy(1);
        break;
    }
    ui->plotter_widget->update();
}

void MainWindow::slot_ColorChooser_CoordinateDiagram()
{
    QColor diagramColor(QColorDialog::getColor(m_color_coordinateLines));
    ui->plotter_widget->setDiagramCoordinateColor(diagramColor);


    QString values=QString("%1,%2,%3").arg(QString::number(diagramColor.red()),
                                           QString::number(diagramColor.green()),
                                           QString::number(diagramColor.blue())
                                           );
    ui->label_showDiagramCoordinateColor->setStyleSheet("background-color: rgb("+values+");");
    m_color_coordinateLines = diagramColor;
}

void MainWindow::slot_ColorChooser_Diagram()
{
    QColor diagramColor(QColorDialog::getColor(m_color_diagram));
    ui->plotter_widget->setDiagramColor(diagramColor);


    QString values=QString("%1,%2,%3").arg(QString::number(diagramColor.red()),
                                           QString::number(diagramColor.green()),
                                           QString::number(diagramColor.blue())
                                           );
    ui->label_showDiagramColor->setStyleSheet("background-color: rgb("+values+");");
    m_color_diagram = diagramColor;
}

void MainWindow::slot_ColorChooser_GridLines()
{
    QColor gridLineColor(QColorDialog::getColor(m_color_grid));
    ui->plotter_widget->setGridLinesColor(gridLineColor);


    QString values=QString("%1,%2,%3").arg(QString::number(gridLineColor.red()),
                                           QString::number(gridLineColor.green()),
                                           QString::number(gridLineColor.blue())
                                           );
    ui->label_showGridColor->setStyleSheet("background-color: rgb("+values+");");
    m_color_grid = gridLineColor;
}

void MainWindow::slot_ColorChooser_PeaksShape()
{
    QColor peaksColor(QColorDialog::getColor(m_color_peaks));
    ui->plotter_widget->setPeaksColor(peaksColor);


    QString values=QString("%1,%2,%3").arg(QString::number(peaksColor.red()),
                                           QString::number(peaksColor.green()),
                                           QString::number(peaksColor.blue())
                                           );
    ui->label_showPeakColor->setStyleSheet("background-color: rgb("+values+");");
    m_color_peaks = peaksColor;
}

void MainWindow::slot_ColorChooser_NavPoint()
{
    QColor pointColor(QColorDialog::getColor(m_color_navPoint));
    ui->plotter_widget->setNavPointColor(pointColor);


    QString values=QString("%1,%2,%3").arg(QString::number(pointColor.red()),
                                           QString::number(pointColor.green()),
                                           QString::number(pointColor.blue())
                                           );
    ui->label_showPointColor->setStyleSheet("background-color: rgb("+values+");");
    m_color_navPoint = pointColor;
}

void MainWindow::slot_CheckBox_SetDiagramCoordinate(bool status)
{
    ui->plotter_widget->setIsShowCoordinateLines(status);
    ui->pushButton_colorChooser_DiagramCoordinate->setEnabled(status);
    QString values;
    QColor pointColor;
    if(status)
    {
        pointColor = m_color_coordinateLines;
    }
    else
    {
        pointColor=QColor(Qt::lightGray);
    }
    values=QString("%1,%2,%3").arg(QString::number(pointColor.red()),
                                   QString::number(pointColor.green()),
                                   QString::number(pointColor.blue())
                                   );
    ui->label_showDiagramCoordinateColor->setStyleSheet("background-color: rgb("+values+");");
    ui->plotter_widget->update();
}

void MainWindow::slot_CheckBox_Navigation(bool status)
{
    ui->plotter_widget->setNavPointShow(status);
    ui->comboBox_pointShape->setEnabled(status);
    ui->pushButton_colorChooser_NavPoint->setEnabled(status);

    QString values;
    QColor pointColor;
    if(status)
    {
        pointColor = m_color_navPoint;
    }
    else
    {
        pointColor=QColor(Qt::lightGray);
    }
    values=QString("%1,%2,%3").arg(QString::number(pointColor.red()),
                                   QString::number(pointColor.green()),
                                   QString::number(pointColor.blue())
                                   );
    ui->label_showPointColor->setStyleSheet("background-color: rgb("+values+");");

    ui->plotter_widget->update();
}

void MainWindow::slot_CheckBox_ShowPeaks(bool status)
{
    ui->plotter_widget->setPeaksShownStatus(status);
    ui->comboBox_peaksShape->setEnabled(status);
    ui->checkBox_showPeaksText->setEnabled(status);
    ui->pushButton_colorChooser_PeakPoint->setEnabled(status);


    QString values;
    QColor pointColor;
    if(status)
    {
        pointColor = m_color_peaks;
    }
    else
    {
        pointColor=QColor(Qt::lightGray);
    }
    values=QString("%1,%2,%3").arg(QString::number(pointColor.red()),
                                   QString::number(pointColor.green()),
                                   QString::number(pointColor.blue())
                                   );
    ui->label_showPeakColor->setStyleSheet("background-color: rgb("+values+");");


    ui->plotter_widget->update();
}

void MainWindow::slot_CheckBox_PeaksValues(bool status)
{
    ui->plotter_widget->setIsPeakTextEnable(status);
    ui->plotter_widget->update();
}

void MainWindow::slot_Button_Save()
{
    QString diagramFileName = QFileDialog::getSaveFileName(this,
                                                           tr("Save Diagram"), ".",
                                                           tr("PNG(*.png);;JPEG (*.jpg);;Microsoft BMP(*.bmp)"));
    if(!diagramFileName.isEmpty())
    {
        bool result = ui->plotter_widget->saveDiagramOnPath(diagramFileName);
        QMessageBox msgBox;


        if(result)
        {
            msgBox.setText("File Saved On:\n "+diagramFileName+"\n Sucessfully.");
        }
        else
        {
            msgBox.setText("Cannot save the file!!!");
        }
        msgBox.exec();
    }
}

void MainWindow::slot_Button_Restore()
{
    QString values;
    m_color_coordinateLines = QColor(Qt::lightGray);
    values=QString("%1,%2,%3").arg(QString::number(m_color_coordinateLines.red()),
                                           QString::number(m_color_coordinateLines.green()),
                                           QString::number(m_color_coordinateLines.blue())
                                           );
    ui->label_showDiagramCoordinateColor->setStyleSheet("background-color: rgb("+values+");");
    m_color_coordinateLines = QColor(Qt::black);/*Default Color*/


    m_color_navPoint = QColor(Qt::lightGray);
    values=QString("%1,%2,%3").arg(QString::number(m_color_navPoint.red()),
                                           QString::number(m_color_navPoint.green()),
                                           QString::number(m_color_navPoint.blue())
                                           );
    ui->label_showPointColor->setStyleSheet("background-color: rgb("+values+");");
    m_color_navPoint = QColor(Qt::blue);/*Default Color*/


    m_color_peaks = QColor(Qt::lightGray);
    values=QString("%1,%2,%3").arg(QString::number(m_color_peaks.red()),
                                           QString::number(m_color_peaks.green()),
                                           QString::number(m_color_peaks.blue())
                                           );
    ui->label_showPeakColor->setStyleSheet("background-color: rgb("+values+");");
    m_color_peaks = QColor(Qt::darkRed);/*Default Color*/

    m_color_diagram = Qt::red;
    values=QString("%1,%2,%3").arg(QString::number(m_color_diagram.red()),
                                           QString::number(m_color_diagram.green()),
                                           QString::number(m_color_diagram.blue())
                                           );
    ui->label_showDiagramColor->setStyleSheet("background-color: rgb("+values+");");

    m_color_grid = Qt::darkGray;
    values=QString("%1,%2,%3").arg(QString::number(m_color_grid.red()),
                                           QString::number(m_color_grid.green()),
                                           QString::number(m_color_grid.blue())
                                           );
    ui->label_showGridColor->setStyleSheet("background-color: rgb("+values+");");


    ui->dial_diagramLevel->setValue(1);
    ui->label_diagramLevel->setText("1");

    ui->comboBox_Grid->setCurrentIndex(1);
    ui->comboBox_Accuracy->setCurrentIndex(0);
    ui->comboBox_LineWidth->setCurrentIndex(2);
    ui->comboBox_peaksShape->setCurrentIndex(1);
    ui->comboBox_pointShape->setCurrentIndex(1);



    ui->checkBox_showPeaks->setCheckState(Qt::Unchecked);
    ui->checkBox_showPeaksText->setCheckState(Qt::Unchecked);
    ui->checkBox_diagramNavigator->setCheckState(Qt::Unchecked);
    ui->checkBox_diagramCoordinate->setCheckState(Qt::Unchecked);


    ui->plotter_widget->setGridLinesColor(m_color_grid);
    ui->plotter_widget->setDiagramColor(m_color_diagram);

    ui->plotter_widget->setPeaksColor(m_color_peaks);
    ui->plotter_widget->setNavPointColor(m_color_navPoint);
    ui->plotter_widget->setDiagramCoordinateColor(m_color_coordinateLines);



    ui->plotter_widget->restoreDiagram();


    ui->plotter_widget->update();

}


void MainWindow::slot_Button_About()
{
    QMessageBox::about(this,tr("About"),tr("<h2>Distribution of shear strain through thickness of 1100 aluminum ARB processed by n cycles</h2>"
                                           "<p>Application released under <strong>GPL v3.0</strong> license so you can use,sell,modify it."
                                           "<br>For more information about GPL please check <a href=""www.gnu.org/licenses"">GNU GPL</a>."
                                           "<br>Also this is an open source software so you can find the source code on <a href=""https://gitlab.com/ali_sarlak/Gamma_Formula/tree/master"">GitLab</a>."
                                           "<p>This application is "
                                           "created by <style>a:link {color: #00AA00}</style> <a href=""http://nano-fa.com/downloads/downloads.php""><strong>NanoFa Company</strong></a> IT department. "
                                           "<p>Release Date : 15 Oct 2016<br>Software version : 16.10.0<p>"
                                           "<p>Copyright &copy; 2016 NanoFa Software Inc."
                                           ));
}

void MainWindow::slot_Button_Help()
{
    bool flag = false;
    QFile file("manual.pdf");
    if(file.exists())
    {
        flag = true;
    }
    else
    {
        flag = QFile::copy(":/docs/manual.pdf","manual.pdf");
    }
    if(flag)
    {
        QDesktopServices::openUrl(QUrl::fromLocalFile("manual.pdf"));
    }
    else
    {
        QMessageBox msgbox;
        msgbox.setText("Cannot Find Manual!!!");
    }

}

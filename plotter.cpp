#include "plotter.h"
#include <QMouseEvent>
#include <QToolTip>

/***************************************************************************************************************/
/*                                             CONSTANT VALUES                                                 */
const int Plotter::DEFAULT_ARRAY_LENGHT = 1000;
const unsigned int Plotter::MAX_ZOOM_LEVEL = 20;
const unsigned int Plotter::TIMER_DURATION = 50;


/***************************************************************************************************************/


/**
 * @brief Plotter::Plotter
 * @param parent
 */
Plotter::Plotter(QWidget *parent) : QWidget(parent)
{
    //static const int GRAY_COLOR = 127;



    /*Best values for m_width & m_height respectively are (401 x 601).*/

    m_diagramHeight = 0.0F;
    m_squareNoMultiplier = 1;


    m_squareNo = 8;
    m_diagramFunctionRange = ((13.6F)*(pow((0.5F),2))) + 288.4F * (pow((0.5F),6));

    m_rubberBand = NULL;

    m_current_array = NULL;
    m_result_array = NULL;
    m_array_lenght = DEFAULT_ARRAY_LENGHT;
    m_accuracy = 1;
    m_n = 1;
    m_lineWidth = 1.0F;
    m_zoomLevel = 1;
    m_zoomFactor = 1.0F;

    m_deltaX = 0.0F;
    m_deltaY = 0.0F;
    m_width = 0.0F;
    m_height = 0.0F;
    this->setDiagramAccuracy(m_accuracy);

    memset(&m_zoomStackEntry,0,sizeof(ZoomStackEntry));

    m_timer = NULL;
    m_timer = new QTimer(this);
    connect(m_timer, SIGNAL(timeout()), this, SLOT(updateTXTCaption()));

    m_pixmap = NULL;
    m_painter = NULL;

    m_isNavigatePointDraw = false;
    m_isPeaksDrawn = false;
    m_navigatePointX = 0.0F;

    m_pointShape = Circle;
    m_peakShape = Circle;



    m_backgroundColor = Qt::white;
    m_diagramCoordinateColor = Qt::black;
    m_navPointColor = Qt::blue;
    m_peaksColor = Qt::darkRed;
    m_gridLinesColor = Qt::darkGray;
    m_diagramColor = Qt::red;

    m_isPeakTextEnable = false;
    m_isMoving = false;
    m_isShowCoordinateLines = false;

    m_maxHorizontalStep = 10.0F;
    m_maxVerticalStep = 12.0F;

}
/***************************************************************************************************************/

/**
 * @brief Plotter::makeDiagramData
 * @n This function is a kind of recursion function that calculate and provide data
 * for diagram in plotter widget.
 * @n
 * @note : This function use of two class member array with specific lenght that is related to accuracy.
 * @param n
 * @n The number of times it must be done, this number must be greater than zero.
 */
void Plotter::makeDiagramData(unsigned int n)
{

    qreal xStep = 1.0F/static_cast<qreal>(m_array_lenght);
    qreal x = 0.0F;
    qreal width = m_diagramRect.width();


    if(n==1)
    {
        /* Gamma Formula For First Step is:
         * 13.6(x-0.5)^2 + 288.4(0.5-x)^6
         * D: [0~1]
         * R: [0~7.90625]
         */

        FAST unsigned int i = 0;

        qreal range = ((13.6F)*(pow((0.5F-x),2))) + 288.4F * (pow((0.5F-x),6));

        qreal width = m_diagramRect.width();
        qreal height = m_diagramRect.height();

        for (i = 0; i < m_array_lenght; ++i)
        {
            m_result_array[i].rx() = x;
            m_result_array[i].rx() *= width;
            m_result_array[i].ry() = ((13.6F)*(pow((0.5F-x),2))) + 288.4F * (pow((0.5F-x),6));
            m_result_array[i].ry() = height -
                    ((m_result_array[i].ry()/range)*m_diagramHeight);
            x+=xStep;
        }
        return;
    }


    makeDiagramData(n-1);
    FAST unsigned int j = 0;
    unsigned int half_array_lenght = (m_array_lenght/2);


    /*Left half*/
    for(j = 0; j < half_array_lenght; ++j)
    {
        m_current_array[j].rx() = x * width;
        m_current_array[j].ry() = m_result_array[j*2].y();
        m_current_array[j].ry() =( m_current_array[j].y()+ m_result_array[j].y())/2.0F;
        x+=xStep;
    }

    /*Right half*/
    /*k start from 1 because Left half end by (half_array_lenght-1)*/
    int k = 1;
    for(j = half_array_lenght; j < m_array_lenght; ++j,k++)
    {
        m_current_array[j].rx() = m_current_array[j-half_array_lenght].rx() +
                (static_cast<float>(width)/2.0F);
        /*[half_array_lenght - k] never less than zero so it's ok*/
        m_current_array[j].ry() = m_current_array[half_array_lenght - k].ry();
    }
    /*Copy the m_current_array into m_result_array for future use*/
    memcpy(m_result_array,m_current_array,m_array_lenght*sizeof(QPointF));
}
/***************************************************************************************************************/
/**
 * @brief Plotter::setIsShowCoordinateLines
 * @n Change the m_isShowCoordinateLines variable and then cause to draw the coordinate lines on diagram.
 * @param isShowCoordinateLines
 * @n m_isShowCoordinateLines <- isShowCoordinateLines
 */
void Plotter::setIsShowCoordinateLines(bool isShowCoordinateLines)
{
    m_isShowCoordinateLines = isShowCoordinateLines;
}
/***************************************************************************************************************/
/**
 * @brief Plotter::setIsPeakTextEnable
 * @n Change the m_isPeakTextEnable variable and then cause to draw the peaks text on diagram.
 * @param isPeakTextEnable
 * @n m_isPeakTextEnable <- isPeakTextEnable
 */
void Plotter::setIsPeakTextEnable(bool isPeakTextEnable)
{
    m_isPeakTextEnable = isPeakTextEnable;
}
/***************************************************************************************************************/
/**
 * @brief Plotter::drawBackgroundGrid
 * @n This is a function that responsible for drawing background grid.
 * @param painter
 * @n Used for painting
 * @param squareNo
 * @n Determine the number of square should be used for minimum side.
 * @note This function is really related to m_width & m_height.
 */
void Plotter::drawBackgroundGrid(QPainter *painter,int squareNo)
{
    if(squareNo==0)
        return;
    /*Save the painter status*/
    painter->save();


    m_pen.setColor(m_gridLinesColor);
    m_pen.setWidthF(1.0F);

    qreal xStep = 0.0F;
    qreal yStep = 0.0F;

    qreal width = m_diagramRect.width();
    qreal height = m_diagramRect.height();

    qreal max = (width>height)?width:height;
    qreal min = (width<height)?width:height;

    float ratio = (max/min);

    /*xStep adn yStep are really dependant to m_with and m_height so becareful about widget dimention*/
    if(width < max)
    {
        xStep = width/static_cast<qreal>(squareNo);
        yStep = height/(static_cast<qreal>(squareNo)*ratio);
    }
    else
    {
        /*(m_width == max)*/
        xStep = height/static_cast<qreal>(squareNo);
        yStep = width/(static_cast<qreal>(squareNo)*ratio);
    }

    painter->setPen(m_pen);
    QPointF point_1;
    QPointF point_2;

    /*Draw vertical lines use xStep*/
    for (qreal x = 0.0F; x < width; x+=xStep)
    {
        point_1.rx()=x+m_deltaX;
        point_1.ry()=0.0F;

        point_2.rx()=x+m_deltaX;
        point_2.ry()=height;

        painter->drawLine(point_1,point_2);
    }

    /*Draw left side if necessary
     *Note: if m_deltaX<0 means the movement is done to the right so I skip the below loop.
     *      o.w loop will be done.
     */
    for (qreal x = m_deltaX-xStep; x > 0.0F; x-=xStep)
    {
        point_1.rx()=x;
        point_1.ry()=0.0F;

        point_2.rx()=x;
        point_2.ry()=height;

        painter->drawLine(point_1,point_2);
    }
    /*Draw right side if necessary
     *Note: if m_deltaX>0 means the movement is done to the left so I skip the below loop.
     *      o.w loop will be done.
     */
    for (qreal x = width; x < width-m_deltaX; x+=xStep)
    {
        point_1.rx()=x+m_deltaX;
        point_1.ry()=0.0F;

        point_2.rx()=x+m_deltaX;
        point_2.ry()=height;

        painter->drawLine(point_1,point_2);
    }
    /*Draw horizontal lines use yStep*/
    for (qreal y = 0.0F; y < height; y+=yStep)
    {
        point_1.rx()=0.0F;
        point_1.ry()=y+m_deltaY;

        point_2.rx()=width;
        point_2.ry()=y+m_deltaY;
        painter->drawLine(point_1,point_2);
    }
    /*Draw top side if necessary
     *Note: if m_deltaY>0 means the movement is done to the bottom so I skip the below loop.
     *      o.w loop will be done.
     */
    for (qreal y = m_deltaY-yStep; y >0.0F; y-=yStep)
    {
        point_1.rx()=0.0F;
        point_1.ry()=y;

        point_2.rx()=width;
        point_2.ry()=y;
        painter->drawLine(point_1,point_2);
    }
    /*Draw bottom side if necessary
     *Note: if m_deltaY<0 means the movement is done to the top so I skip the below loop.
     *      o.w loop will be done.
     */
    for (qreal y = height; y < height - m_deltaY; y+=yStep)
    {
        point_1.rx()=0.0F;
        point_1.ry()=y+m_deltaY;

        point_2.rx()=width;
        point_2.ry()=y+m_deltaY;
        painter->drawLine(point_1,point_2);
    }
    /*to solve drawing last horizontal and vertical line*/
    m_pen.setWidth(2);
    painter->setPen(m_pen);
    painter->drawRect(0,0,width,height);


    painter->restore();
    /*Restore the painter status*/
}
/***************************************************************************************************************/
/**
 * @brief Plotter::drawCoordinateLines
 * @n Draw the coordinate lines (V,H) line and scaled them.
 * @param painter
 * @n painter is used to draw bare bone shapes.
 */
void Plotter::drawCoordinateLines(QPainter *painter)
{
    /*Set the pen*/
    m_pen.setColor(m_diagramCoordinateColor);
    m_pen.setWidth(2);
    painter->setPen(m_pen);

    QString msg;

    QFont msgFont("Arial", 9, QFont::Normal);

    painter->setFont(msgFont);

    QFontMetrics fontMetric(msgFont);

    int fontWidth = 0;
    int fontHeight = 0;

    int txt_x = 0;
    int txt_y = 0;

    QPointF txtPoint;

    fontHeight = fontMetric.height();

    /*Vertical Line*/
    painter->drawLine(m_diagramRect.left(),0,m_diagramRect.left(),m_height);
    qreal stepY = m_diagramRect.height() / m_maxVerticalStep;
    qreal yValue =((m_n*m_diagramFunctionRange)/m_maxVerticalStep)+(ONE_QUARTER_PORTION*m_n*m_diagramFunctionRange);

    /*From Origin to +infinity*/
    for (qreal y = m_diagramRect.top()+ m_diagramRect.height()-stepY; y >0 ; y-=stepY)
    {
        painter->drawLine(m_diagramRect.left()-2,y,m_diagramRect.left() + 2,y);
        msg=QString("%1").arg(QString::number(yValue,'f',2));
        fontWidth = fontMetric.width(msg);
        txt_x = (m_diagramRect.left())+(fontWidth/4.0F);
        txt_y = (y)+(fontHeight/4.0);
        txtPoint.rx() = txt_x;
        txtPoint.ry() = txt_y;
        painter->drawText(txtPoint,msg);
        yValue+=((m_n*m_diagramFunctionRange)/m_maxVerticalStep);
    }

    stepY = m_diagramRect.height() / m_maxVerticalStep;
    /*(ONE_QUARTER_PORTION*m_n*m_diagramFunctionRange) is a compensation of m_diagramHeight*/
    yValue = -((m_n*m_diagramFunctionRange)/m_maxVerticalStep)-(ONE_QUARTER_PORTION*m_n*m_diagramFunctionRange);
    /*From Origin to -infinity*/
    for (qreal y =m_diagramRect.top()+ m_diagramRect.height()+stepY; y <m_height ; y+=stepY)
    {
        painter->drawLine(m_diagramRect.left()-2,y,m_diagramRect.left() + 2,y);
        msg=QString("%1").arg(QString::number(yValue,'f',2));
        fontWidth = fontMetric.width(msg);
        txt_x = (m_diagramRect.left())+(fontWidth/4.0F);
        txt_y = (y)+(fontHeight/4.0F);
        txtPoint.rx() = txt_x;
        txtPoint.ry() = txt_y;
        painter->drawText(txtPoint,msg);
        yValue-=((m_n*m_diagramFunctionRange)/m_maxVerticalStep);
    }


    /*Horizontal Line*/
    painter->drawLine(0,m_diagramRect.top()+m_diagramRect.height(),m_width,m_diagramRect.top()+m_diagramRect.height());

    qreal stepX = m_diagramRect.width() / m_maxHorizontalStep;
    qreal xValue =0.0F;
    /*From Origin to +infinity*/
    for (qreal x = m_diagramRect.left(); x < m_diagramRect.left()+ m_diagramRect.width()-m_deltaX; x+=stepX)
    {
        painter->drawLine(x,m_diagramRect.top()+m_diagramRect.height()-2,x,m_diagramRect.top()+m_diagramRect.height()+2);
        msg=QString("%1").arg(QString::number(xValue,'f',2));
        fontWidth = fontMetric.width(msg);

        txt_x = (x)-(fontWidth/2.0F);
        txt_y = (m_diagramRect.top()+m_diagramRect.height())-(fontHeight/2.0F);
        txtPoint.rx() = txt_x;
        txtPoint.ry() = txt_y;
        painter->drawText(txtPoint,msg);
        xValue+=(1.0F/m_maxHorizontalStep);
    }
    xValue = 0.0F;
    xValue-=(1.0F/m_maxHorizontalStep);
    /*From Origin to -infinity*/
    for (qreal x =m_diagramRect.left()-stepX; x >0 ; x-=stepX)
    {
        painter->drawLine(x,m_diagramRect.top()+m_diagramRect.height()-2,x,m_diagramRect.top()+m_diagramRect.height()+2);
        msg=QString("%1").arg(QString::number(xValue,'f',2));
        fontWidth = fontMetric.width(msg);

        txt_x = (x)-(fontWidth/2.0F);
        txt_y = (m_diagramRect.top()+m_diagramRect.height())-(fontHeight/2.0F);
        txtPoint.rx() = txt_x;
        txtPoint.ry() = txt_y;
        painter->drawText(txtPoint,msg);
        xValue-=(1.0F/m_maxHorizontalStep);
    }

}
/***************************************************************************************************************/

/**
 * @brief Plotter::drawDiagram
 * @n This function is responsible for draw the final gamma formula according to makeDiagramData function.
 * @param painter
 * @n painter is used for draw objects
 * @param n
 * @n Time of repeat.
 */
void Plotter::drawDiagram(QPainter *painter, unsigned int n)
{
    makeDiagramData(n);
    painter->save();


    m_pen.setColor(m_diagramColor);
    m_pen.setWidthF(m_lineWidth);
    painter->setPen(m_pen);


    FAST unsigned int i=0;
    for (i = 0; i < m_array_lenght; ++i)
    {
        m_result_array[i].rx() += m_deltaX;
        m_result_array[i].ry() += m_deltaY;
    }

    painter->drawPolyline(m_result_array,m_array_lenght);

    painter->restore();
}


/**
 * @brief Plotter::drawZoomMSG
 * @param painter
 */
void Plotter::drawZoomMSG(QPainter *painter)
{
    /*Show the messages in the middle of plotter*/
    painter->save();

    qreal x=0.0F;
    qreal y=0.0F;

    QString msg;
    QColor msgColor;
    if(m_zoomLevel>=MAX_ZOOM_LEVEL)
    {
        msg="MAX ZOOM REACHED";
        msgColor.setRgb(0,127,0,m_textAlpha);
    }
    if(m_zoomLevel==1)
    {
        msg="MIN ZOOM REACHED";
        msgColor.setRgb(0,0,255,m_textAlpha);
    }


    QFont msgFont("Arial", 10, QFont::Bold);

    painter->setFont(msgFont);

    QFontMetrics fontMetric(msgFont);

    int fontWidth = fontMetric.width(msg);
    int fontHeight = fontMetric.height();

    x = (m_width/2.0F)-(fontWidth/2.0F);
    y = (m_height/2.0F)-(fontHeight/2.0F);

    QPointF txtPoint(x,y);

    m_pen.setColor(msgColor);
    painter->setPen(m_pen);
    painter->drawText(txtPoint,msg);

    painter->restore();
}


/**
 * @brief Plotter::drawPointOnDiagram
 * @n Draw a point on diagram acording to mouse position
 * @param painter
 * @n painter object for draw
 * @param x
 * @n The x position to calculate the y position of appropriate point.
 */
void Plotter::drawPointOnDiagram(QPainter *painter)
{

    qreal x = (m_navigatePointX-m_deltaX)/static_cast<qreal>(m_diagramRect.width());
    /*x cause to calculate y*/
    if(x>1.0F || x<0.0F)
        return;

    unsigned int lowIndex = static_cast<unsigned int>(floorf(x* static_cast<float>(m_array_lenght)));
    unsigned int topIndex = static_cast<unsigned int>(ceilf(x* static_cast<float>(m_array_lenght)));

    QPointF center;
    QPointF p1;
    QPointF p2;
    qreal y=0.0F;
    QPointF Triangle_Points[3];
    QPointF Square_Points[4];
    static const qreal GAP = 4.0F;

    if(lowIndex<topIndex)
    {
        p1.rx() = m_result_array[lowIndex].x() - m_deltaX;
        p1.ry() = m_result_array[lowIndex].y() - m_deltaY;

        p2.rx() = m_result_array[topIndex].x() - m_deltaX;
        p2.ry() = m_result_array[topIndex].y() - m_deltaY;

        /*Calculate the hit point of virtual vertical line by line <p1-p2> */
        qreal slope = (p2.y()-p1.y())/(p2.x()-p1.x());
        x = m_navigatePointX - m_deltaX;

        y =(slope*(x-p1.x()))+p1.y();

        center.rx() = x + m_deltaX;
        center.ry() = y + m_deltaY;

        x /= m_diagramRect.width();
        y = (y/(m_diagramRect.height()))*(m_n*m_diagramFunctionRange);
    }
    else
    {
        center.rx()=m_result_array[lowIndex].x();
        center.ry()=m_result_array[lowIndex].y();

        x = center.x() - m_deltaX;
        y = center.y() - m_deltaY;

        x /= m_diagramRect.width();
        y = (y/(m_diagramRect.height()))*(m_n*m_diagramFunctionRange);

    }

    QBrush brush(m_navPointColor,Qt::SolidPattern);
    painter->setBrush(brush);
    m_pen.setColor(m_navPointColor);
    painter->setPen(m_pen);


    switch (m_pointShape)
    {
    case Circle:
        painter->drawEllipse((center),GAP,GAP);
        break;
    case Triangle:
        Triangle_Points[0]=QPointF(center.x()-GAP,center.y()+GAP);
        Triangle_Points[1]=QPointF(center.x()+GAP,center.y()+GAP);
        Triangle_Points[2]=QPointF(center.x(),center.y()-GAP);

        painter->drawPolygon(Triangle_Points,3);
        break;
    case Square:

        Square_Points[0]=QPointF(center.x()-GAP,center.y()+GAP);
        Square_Points[1]=QPointF(center.x()+GAP,center.y()+GAP);
        Square_Points[2]=QPointF(center.x()+GAP,center.y()-GAP);
        Square_Points[3]=QPointF(center.x()-GAP,center.y()-GAP);
        painter->drawPolygon(Square_Points,4);
        break;
    default:
        painter->drawEllipse((center),GAP,GAP);
        break;
    }

    QString msg=QString("(%1,%2)").arg(QString::number(x,'f',3),QString::number((m_n*m_diagramFunctionRange)-y,'f',3));

    QFont msgFont("Arial", 10, QFont::Bold);

    painter->setFont(msgFont);

    QFontMetrics fontMetric(msgFont);

    int fontWidth = fontMetric.width(msg);
    int fontHeight = fontMetric.height();

    int txt_x = (center.x())-(fontWidth/2.0F);
    int txt_y = (center.y())-(fontHeight/2.0F);

    QPointF txtPoint(txt_x,txt_y);

    m_pen.setColor(Qt::darkGreen);
    painter->setPen(m_pen);
    painter->drawText(txtPoint,msg);


}
/***************************************************************************************************************/

/**
 * @brief Plotter::drawDiagramPeaks
 * @n Draw diagram peaks
 * @param painter
 * @n painter parameter is used to draw peaks.
 */
void Plotter::drawDiagramPeaks(QPainter *painter)
{
    static const qreal GAP = 4.0F;
    QBrush brush(Qt::black,Qt::NoBrush);
    painter->setBrush(brush);
    m_pen.setWidth(1);
    m_pen.setColor(m_peaksColor);
    painter->setPen(m_pen);


    QPointF Triangle_Points[3];
    QPointF Square_Points[4];
    qreal deltaY = 0.0F;
    int current_signedY = -1;
    int previous_signedY = -1;
    QPointF peak;

    peak.rx() = m_result_array[0].x();
    peak.ry() = m_result_array[0].y();


    /*Prepare font for draw font on the diagram*/
    QString msg;
    QFont msgFont("Arial", 8, QFont::Normal);
    painter->setFont(msgFont);
    QFontMetrics fontMetric(msgFont);



    int fontWidth = 0;
    int fontHeight = 0;

    int txt_x = 0;
    int txt_y = 0;

    QPointF txtPoint(0.0F,0.0F);

    qreal x = 0.0F;
    qreal y = 0.0F;




    FAST unsigned int index = 0;
    for (index = 1; index < (m_array_lenght); ++index)
    {
        deltaY = peak.y() - m_result_array[index].y();
        current_signedY = (deltaY<0.0F)?-1:+1;


        if((current_signedY != previous_signedY) || (deltaY==0.0F))
        {
            /*Peak Detect*/
            x= peak.x();
            y= peak.y();
            switch (m_peakShape)
            {
            case Circle:
                painter->drawEllipse((peak),GAP,GAP);
                break;
            case Triangle:
                Triangle_Points[0]=QPointF(x-GAP,y+GAP);
                Triangle_Points[1]=QPointF(x+GAP,y+GAP);
                Triangle_Points[2]=QPointF(x,y-GAP);

                painter->drawPolygon(Triangle_Points,3);
                break;
            case Square:

                Square_Points[0]=QPointF(x-GAP,y+GAP);
                Square_Points[1]=QPointF(x+GAP,y+GAP);
                Square_Points[2]=QPointF(x+GAP,y-GAP);
                Square_Points[3]=QPointF(x-GAP,y-GAP);
                painter->drawPolygon(Square_Points,4);
                break;
            default:
                painter->drawEllipse((peak),GAP,GAP);
                break;
            }
            /*Draw point values*/
            if(m_isPeakTextEnable)
            {
                msg=QString("{%1,%2}").arg(QString::number(((x-m_deltaX)/m_diagramRect.width()),'f',2),
                                           QString::number((m_n*m_diagramFunctionRange)-(((y-m_deltaY)/m_diagramRect.height())*(m_n*m_diagramFunctionRange)),'f',2));

                fontWidth = fontMetric.width(msg);
                fontHeight = fontMetric.height();
                txt_x = (x)-(fontWidth/2.0F);
                txt_y = (y)-(fontHeight/2.0F);
                txtPoint.rx() = txt_x;
                txtPoint.ry() = txt_y;


                painter->drawText(txtPoint,msg);
            }

            previous_signedY = current_signedY;
        }
        peak.rx() = m_result_array[index].x();
        peak.ry() = m_result_array[index].y();
    }
    /*O(n)*/

    /*First & Last Point are peak points*/

    /*First*/
    peak.rx() = m_result_array[0].x();
    peak.ry() = m_result_array[0].y();
    x= peak.x();
    y= peak.y();
    switch (m_peakShape)
    {
    case Circle:
        painter->drawEllipse((peak),GAP,GAP);
        break;
    case Triangle:
        Triangle_Points[0]=QPointF(x-GAP,y+GAP);
        Triangle_Points[1]=QPointF(x+GAP,y+GAP);
        Triangle_Points[2]=QPointF(x,y-GAP);

        painter->drawPolygon(Triangle_Points,3);
        break;
    case Square:

        Square_Points[0]=QPointF(x-GAP,y+GAP);
        Square_Points[1]=QPointF(x+GAP,y+GAP);
        Square_Points[2]=QPointF(x+GAP,y-GAP);
        Square_Points[3]=QPointF(x-GAP,y-GAP);
        painter->drawPolygon(Square_Points,4);
        break;
    default:
        painter->drawEllipse((peak),GAP,GAP);
        break;
    }
    /*Draw point values*/
    if(m_isPeakTextEnable)
    {
        msg=QString("{%1,%2}").arg(QString::number((0.0F),'f',2),
                                   QString::number((m_n*m_diagramFunctionRange),'f',2));

        fontWidth = fontMetric.width(msg);
        fontHeight = fontMetric.height();
        txt_x = (x)-(fontWidth/2.0F);
        txt_y = (y)-(fontHeight/2.0F);
        txtPoint.rx() = txt_x;
        txtPoint.ry() = txt_y;
        painter->drawText(txtPoint,msg);
    }

    /*Last*/
    peak.rx() = m_result_array[m_array_lenght-1].x();
    peak.ry() = m_result_array[m_array_lenght-1].y();
    x= peak.x();
    y= peak.y();
    switch (m_peakShape)
    {
    case Circle:
        painter->drawEllipse((peak),GAP,GAP);
        break;
    case Triangle:
        Triangle_Points[0]=QPointF(x-GAP,y+GAP);
        Triangle_Points[1]=QPointF(x+GAP,y+GAP);
        Triangle_Points[2]=QPointF(x,y-GAP);

        painter->drawPolygon(Triangle_Points,3);
        break;
    case Square:

        Square_Points[0]=QPointF(x-GAP,y+GAP);
        Square_Points[1]=QPointF(x+GAP,y+GAP);
        Square_Points[2]=QPointF(x+GAP,y-GAP);
        Square_Points[3]=QPointF(x-GAP,y-GAP);
        painter->drawPolygon(Square_Points,4);
        break;
    default:
        painter->drawEllipse((peak),GAP,GAP);
        break;
    }
    /*Draw point values*/
    if(m_isPeakTextEnable)
    {
        msg=QString("{%1,%2}").arg(QString::number((1.0F),'f',2),
                                   QString::number((m_n*m_diagramFunctionRange),'f',2));

        fontWidth = fontMetric.width(msg);
        fontHeight = fontMetric.height();
        txt_x = (x)-(fontWidth/2.0F);
        txt_y = (y)-(fontHeight/2.0F);
        txtPoint.rx() = txt_x;
        txtPoint.ry() = txt_y;
        painter->drawText(txtPoint,msg);
    }


}
/***************************************************************************************************************/


void Plotter::paintEvent(QPaintEvent *)
{
    if(m_painter==NULL || m_pixmap==NULL)
        return;

    QPainter plotterPainter(this);
    m_painter->eraseRect(0,0,m_width,m_height);

    drawBackgroundGrid(m_painter,m_squareNoMultiplier*m_squareNo);

    drawDiagram(m_painter,m_n);

    if(m_isShowCoordinateLines)
    {
        drawCoordinateLines(m_painter);
    }

    if(m_textAlpha>0)
        drawZoomMSG(m_painter);

    if(m_isNavigatePointDraw)
        drawPointOnDiagram(m_painter);

    if(m_isPeaksDrawn)
        drawDiagramPeaks(m_painter);


    if(m_pixmap!=NULL && m_isMoving)
    {
        plotterPainter.drawPixmap(m_mouseRelocation.x(),m_mouseRelocation.y(),m_width,m_height,*m_pixmap);
    }
    if(m_pixmap!=NULL && !m_isMoving)
    {
        plotterPainter.drawPixmap(0,0,m_width,m_height,*m_pixmap);
    }
    QBrush brush(m_backgroundColor,Qt::NoBrush);
    m_painter->setBrush(brush);

}
/***************************************************************************************************************/


/**
 * @brief Plotter::setDimention
 * @n Set the m_width,m_height and also m_diagramHeight that is used for drawing diagram.
 * @note m_diagramHeight is a portion of height because of visual reasons we use this variable instead of m_height.
 * @param width
 * @param height
 */
void Plotter::setDimention(qreal width, qreal height)
{
    m_diagramHeight = THREE_QUARTER_PORTION * height;
    m_diagramRect.setSize(QSizeF(width,height));
    m_width = width;
    m_height = height;
    if(m_pixmap!=NULL)
    {
        delete m_pixmap;
    }
    m_pixmap = NULL;
    m_pixmap = new QPixmap(m_width,m_height);


    if(m_painter!=NULL)
    {
        m_painter->end();
        delete m_painter;
    }
    m_painter = NULL;
    m_painter = new QPainter(m_pixmap);

    /*Set the painter status for operate in antialise mode*/
    m_painter->setRenderHint(QPainter::Antialiasing,true);

    QBrush brush(m_backgroundColor,Qt::NoBrush);

    m_painter->setBrush(brush);
    m_painter->eraseRect(0,0,m_width,m_height);

}
/***************************************************************************************************************/

/**
 * @brief Plotter::setGridBGSquareNo
 * @n Set the m_squareNoMultiplier variable to influence the value of m_squareNo.
 * @param multiplier
 * @n Input parameter that store in m_squareNoMultiplier.
 */
void Plotter::setGridBGSquareNo(int multiplier)
{
    m_squareNoMultiplier = multiplier;
}
/***************************************************************************************************************/


/**
 * @brief Plotter::setDiagramAccuracy
 * @n This function set the accuracy of the diagram in multiplied of the constant of ARRAY_LENGHT (which is 1000 by default).
 * @n Also this function is responsible for changing the m_array_lenght and allocating memory for two arrays names m_current_array & m_previous_array.
 * @n
 * @note:Becareful about the acuuracy that you would like to pass to this function, the accuracy must be greater than zero.
 * @param accuracy
 * @n Input parameter of setDiagramAccuracy that set the m_accuracy as a memeber of the Plotter class.
 */
void Plotter::setDiagramAccuracy(unsigned int accuracy)
{
    if(accuracy<=0)
    {
        /*It's impossible that accuracy is less than zero due to unsigned in
         * but any way I check this condition.
         */
        m_accuracy = 1;
    }
    else
    {
        m_accuracy = accuracy;
    }
    m_array_lenght = m_accuracy * DEFAULT_ARRAY_LENGHT ;

    /*I use the malloc because I don't want to use the constructor of the QPointF class and also
     *I don't want to initialized the etries by zero so I attemptto get require memory ASAP.
     */
    if(!m_current_array)
    {
        free(m_current_array);
    }
    m_current_array = NULL;
    m_current_array = static_cast<QPointF*>(malloc((sizeof(QPointF)) * m_array_lenght));

    if(!m_result_array)
    {
        free(m_result_array);
    }
    m_result_array = NULL;
    m_result_array = static_cast<QPointF*>(malloc((sizeof(QPointF) * m_array_lenght)));

}
/***************************************************************************************************************/
void Plotter::setPointShape(unsigned int pointShape)
{
    switch (pointShape)
    {
    case 0:
        /*Triangle*/
        m_pointShape = Triangle;
        break;
    case 1:
        /*Circle*/
        m_pointShape = Circle;
        break;
    case 2:
        /*Square*/
        m_pointShape = Square;
        break;
    default:
        m_pointShape = Circle;
        break;
    }
}
/***************************************************************************************************************/
/**
 * @brief Plotter::setDiagramLevel
 * @n Set the diagram m_n to n
 * @param n
 * @n Input parameter
 */
void Plotter::setDiagramLevel(unsigned int n)
{
    m_n = n;
}

/**
 * @brief Plotter::setDiagramCoordinateColor
 * @n Set the coordinate lines and text color.
 * @param color
 * @n m_diagramCoordinateColor <- color;
 */
void Plotter::setDiagramCoordinateColor(QColor color)
{
    m_diagramCoordinateColor = color;
}
/***************************************************************************************************************/

/**
 * @brief Plotter::setDiagramColor
 * @n Set the color of the diagram to your desire color.
 * @param color
 * @n Input color that will set the member attribute of plotter class name m_color.
 */
void Plotter::setDiagramColor(QColor color)
{
    m_diagramColor = color;
}
/***************************************************************************************************************/
/**
 * @brief Plotter::setGridLinesColor
 * @n Cause to change m_gridLinesColor variable.
 * @param color
 * @n m_gridLinesColor <- color
 */
void Plotter::setGridLinesColor(QColor color)
{
    m_gridLinesColor = color;
}
/***************************************************************************************************************/
/**
 * @brief Plotter::setPeaksColor
 * @n Cause to change m_peaksColor variable.
 * @param color
 * @n m_peaksColor <- color
 */
void Plotter::setPeaksColor(QColor color)
{
    m_peaksColor = color;
}
/***************************************************************************************************************/
/**
 * @brief Plotter::setNavPointColor
 * @n Cause to change m_navPointColor variable.
 * @param color
 * @n m_navPointColor <- color
 */
void Plotter::setNavPointColor(QColor color)
{
    m_navPointColor = color;
}
/***************************************************************************************************************/


/**
 * @brief Plotter::setLineWidth
 * @n Set the line width of diagram.
 * @param lineWidth
 * @n Input parameter.
 */
void Plotter::setLineWidth(qreal lineWidth)
{
    m_lineWidth = lineWidth;
}

/**
 * @brief Plotter::setNavPointShow
 * @n Cause to show the point navigator on the diagram.
 * @param isShown
 * @n Replace with m_isNavigatePointDraw value.
 */
void Plotter::setNavPointShow(bool isShown)
{
    m_isNavigatePointDraw = isShown;
}
/***************************************************************************************************************/

/**
 * @brief Plotter::setPeakShape
 * @n Determine the shape of the peaks shape
 * @param pointShape
 * @n Input paameter that interpret to PointShape enumeration.
 */
void Plotter::setPeakShape(unsigned int pointShape)
{
    switch (pointShape)
    {
    case 0:
        /*Triangle*/
        m_peakShape = Triangle;
        break;
    case 1:
        /*Circle*/
        m_peakShape = Circle;
        break;
    case 2:
        /*Square*/
        m_peakShape = Square;
        break;
    default:
        m_peakShape = Circle;
        break;
    }
}
/***************************************************************************************************************/

/**
 * @brief Plotter::setPeaksShownStatus
 * @n Change the m_isPeaksDrawn variable change and then cause to draw the peaks on diagram.
 * @param isPeakShown
 * @n Input parameter that change the m_isPeaksDrawn status.
 */
void Plotter::setPeaksShownStatus(bool isPeakShown)
{
    m_isPeaksDrawn = isPeakShown;
}
/***************************************************************************************************************/

/**
 * @brief Plotter::saveDiagramOnPath
 * @n Save diagram image on file
 * @param path
 * @n The path of file.
 */
bool Plotter::saveDiagramOnPath(QString path)
{
    if(m_pixmap!=NULL)
    {
        return m_pixmap->save(path);
    }
    return false;
}
/***************************************************************************************************************/

/**
 * @brief Plotter::restoreDiagram
 * @n Restore all data which is related to diagram and relocate it on basic status.
 */
void Plotter::restoreDiagram()
{
    m_deltaX = 0.0F;
    m_deltaY = 0.0F;
    m_diagramRect.setX(0.0F);
    m_diagramRect.setY(0.0F);
    m_diagramRect.setSize(QSizeF(m_width,m_height));

    m_diagramHeight = THREE_QUARTER_PORTION * m_height;
    m_n = 1;
    m_accuracy = 1;
    m_array_lenght = m_accuracy * DEFAULT_ARRAY_LENGHT;


    m_isNavigatePointDraw = false;
    m_isPeaksDrawn = false;
    m_navigatePointX = 0.0F;

    m_pointShape = Circle;
    m_peakShape = Circle;

    m_backgroundColor = Qt::white;
    m_diagramCoordinateColor = Qt::black;
    m_navPointColor = Qt::blue;
    m_peaksColor = Qt::darkRed;
    m_gridLinesColor = Qt::darkGray;
    m_diagramColor = Qt::red;

    m_isPeakTextEnable = false;
    m_isMoving = false;
    m_isShowCoordinateLines = false;

    m_maxHorizontalStep = 10.0F;
    m_maxVerticalStep = 12.0F;

    m_zoomStack.clear();
    m_lineWidth = 1.0F;
    m_zoomLevel = 1;
    m_zoomFactor = 1.0F;

    m_squareNoMultiplier = 1;
    m_squareNo = 8;
    m_diagramFunctionRange = ((13.6F)*(pow((0.5F),2))) + 288.4F * (pow((0.5F),6));

    this->setDiagramAccuracy(m_accuracy);

    memset(&m_zoomStackEntry,0,sizeof(ZoomStackEntry));


}
/***************************************************************************************************************/



/*================================= Mouse Actions =====================================*/

void Plotter::mousePressEvent(QMouseEvent *event)
{
    m_mouseStartPoint = event->pos();
}
/***************************************************************************************************************/

void Plotter::mouseMoveEvent(QMouseEvent *event)
{
    qreal width = m_diagramRect.width();
    qreal height = m_diagramRect.height();

    QRect plotter_widget(0,0,width,height);




    if(event->buttons()==Qt::LeftButton)
    {
        if(m_rubberBand==NULL)
        {
            m_rubberBand = new QRubberBand(QRubberBand::Rectangle,this);
        }
        QRect rubberBandArea(m_mouseStartPoint, event->pos());
        if(rubberBandArea.width()> width)
        {
            rubberBandArea.setWidth(width);
        }
        if(rubberBandArea.height()> height)
        {
            rubberBandArea.setHeight(height);
        }
        rubberBandArea = rubberBandArea.normalized();

        //fixme : dynamic change to rubberband
        QPalette pal;
        pal.setBrush(QPalette::Highlight, QBrush(Qt::green));

        m_rubberBand->setPalette(pal);
        m_rubberBand->setGeometry(rubberBandArea);

        if(plotter_widget.contains(event->pos(),false))
        {
            QToolTip::showText(event->globalPos(),QString("(%1,%2)")
                               .arg(m_rubberBand->width()/width)
                               .arg((m_rubberBand->height()/height)*(m_n*m_diagramFunctionRange)),this);
        }
        else
        {
            QToolTip::hideText();
        }

        m_rubberBand->show();

    }

    if(event->buttons()==Qt::RightButton)
    {
        m_mouseRelocation.rx() = event->x() - m_mouseStartPoint.x();
        m_mouseRelocation.ry() = event->y() - m_mouseStartPoint.y();
        m_isMoving = true;
        this->update();
    }

    if(m_isNavigatePointDraw)
    {
        m_navigatePointX = static_cast<qreal>(event->x());

        this->update();

    }

}
/***************************************************************************************************************/

void Plotter::mouseReleaseEvent(QMouseEvent *)
{
    if(m_rubberBand!=NULL)
    {
        m_rubberBand->hide();
    }


    m_deltaX += m_mouseRelocation.x();
    m_deltaY += m_mouseRelocation.y();
    QSizeF previousSize(m_diagramRect.width(),m_diagramRect.height());
    m_diagramRect.setX(m_deltaX);
    m_diagramRect.setY(m_deltaY);
    m_diagramRect.setSize(previousSize);
    m_mouseRelocation.rx() = 0;
    m_mouseRelocation.ry() = 0;
    m_isMoving = false;

    this->update();

}
/***************************************************************************************************************/

/**
 * @brief Plotter::wheelEvent
 * @n Cause Zoom In and Zoom Out
 * @n @note Zoom Out will be done by stack and undo the zoom in operation.
 * @param event
 */
void Plotter::wheelEvent(QWheelEvent *event)
{
    qreal width = m_width;
    qreal height = m_height;

    if(event->delta()>0)
    {
        /*Zoom In*/
        if((m_zoomLevel>MAX_ZOOM_LEVEL))
        {
            /*Show "MAX ZOOM LEVEL REACHED"*/
            if(m_timer)
                m_timer->start(TIMER_DURATION);
            m_textAlpha = 255;
            return;
        }
        m_zoomLevel++;
        /*Push to stack*/
        m_zoomStackEntry.deltaX = m_deltaX;
        m_zoomStackEntry.deltaY = m_deltaY;
        m_zoomStackEntry.width = m_diagramRect.width();
        m_zoomStackEntry.height = m_diagramRect.height();
        m_zoomStackEntry.zoomFactor = m_zoomFactor;
        m_zoomStack.push(m_zoomStackEntry);
        /*Push to stack*/


        QPointF mousePosition = event->posF();
        //mousePosition.rx() = mousePosition.x()/this->x();

        qreal newX=0.0F;
        qreal newY=0.0F;

        m_zoomFactor *= 2.0F;

        newX = ((m_diagramRect.left() -
                 mousePosition.x())*(width * m_zoomFactor))/m_diagramRect.width() ;
        newY = ((m_diagramRect.top() -
                 mousePosition.y()) *(height * m_zoomFactor))/m_diagramRect.height();

        m_deltaX = mousePosition.x() + newX;
        m_deltaY = mousePosition.y() + newY;


        height *= m_zoomFactor;
        width *= m_zoomFactor;

        m_diagramRect.setX(m_deltaX);
        m_diagramRect.setY(m_deltaY);

        m_diagramHeight = THREE_QUARTER_PORTION * height;
        m_diagramRect.setSize(QSizeF(width,height));



    }
    else
    {
        /*Zoom Out*/
        if(m_zoomStack.isEmpty())
        {
            /*MIN LEVEL ZOOM REACHED*/
            if(m_timer)
                m_timer->start(TIMER_DURATION);
            m_textAlpha = 255;
            return;
        }

        m_zoomStackEntry = m_zoomStack.pop();
        m_deltaX = m_zoomStackEntry.deltaX;
        m_deltaY = m_zoomStackEntry.deltaY;

        m_diagramRect.setX(m_deltaX);
        m_diagramRect.setY(m_deltaY);
        height =m_zoomStackEntry.height;
        width =m_zoomStackEntry.width;

        m_zoomFactor = m_zoomStackEntry.zoomFactor;

        m_diagramHeight = THREE_QUARTER_PORTION * height;
        m_diagramRect.setSize(QSizeF(width,height));
        /*m_zoomLevel never be zero*/
        m_zoomLevel--;
    }

    this->update();
}
/***************************************************************************************************************/
/**
 * @brief Plotter::~Plotter This is a Destructor of class
 */
Plotter::~Plotter()
{
    if(!m_rubberBand)
    {
        delete m_rubberBand;
    }
    m_rubberBand = NULL;

    if(!m_current_array)
    {
        free(m_current_array);
    }
    m_current_array = NULL;

    if(!m_result_array)
    {
        free(m_result_array);
    }
    m_result_array = NULL;

    if(m_painter!=NULL)
    {
        m_painter->end();
        delete m_painter;
    }
    m_painter = NULL;

    if(m_pixmap!=NULL)
    {
        delete m_pixmap;
    }
    m_pixmap = NULL;


    if(!m_timer)
    {
        if(m_timer->isActive())
        {
            m_timer->stop();
        }
        delete m_timer;
    }
    m_timer=NULL;


}
/***************************************************************************************************************/

/**
 * @brief Plotter::updateTXTCaption
 * @n Cause to call drawZoomMSG and show MAX ZOOM REACHED & MIN ZOOM REACHED then smoothly hidden the text.
 */
void Plotter::updateTXTCaption()
{
    m_textAlpha-=10;

    if(m_textAlpha<0)
    {
        m_timer->stop();
        m_textAlpha = 0;
    }
    this->update();
}
/***************************************************************************************************************/



#-------------------------------------------------
#
# Project created by QtCreator 2016-08-18T11:08:37
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4):
QT += widgets

TARGET = Gamma_Formula
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    plotter.cpp

HEADERS  += mainwindow.h \
    plotter.h

FORMS    += mainwindow.ui


CONFIG += c++11

RESOURCES += \
    resources.qrc

RC_FILE = gamma.rc

#ifndef PLOTTER_H
#define PLOTTER_H

#include <QObject>
#include <QWidget>
#include <QPainter>
#include <QRubberBand>
#include <QStack>
#include <QTimer>
#include <QGraphicsScene>



/**
 * @brief The Plotter class
 */

#define FAST register
#define THREE_QUARTER_PORTION (3.0F/4.0F)
#define ONE_QUARTER_PORTION (1.0F/4.0F)
#define EPSILON 0.00000001F



class Plotter : public QWidget
{
    Q_OBJECT
private :
    /*CONSTANT VALUES*/
    static const int DEFAULT_ARRAY_LENGHT;
    static const unsigned int MAX_ZOOM_LEVEL;
    static const unsigned int TIMER_DURATION;


private:
    QColor m_backgroundColor;
    QColor m_diagramColor;
    QColor m_gridLinesColor;
    QColor m_navPointColor;
    QColor m_peaksColor;
    QColor m_diagramCoordinateColor;


    QPainter *m_painter;
    QPixmap *m_pixmap;

    typedef enum EShape
    {
        Triangle=0,
        Circle,
        Square
    }DiagramShape;

    DiagramShape m_pointShape;
    DiagramShape m_peakShape;

    qreal m_lineWidth;
    unsigned int m_zoomLevel;
    qreal m_zoomFactor;
    qreal m_width;
    qreal m_height;

    qreal m_maxHorizontalStep;
    qreal m_maxVerticalStep;

    qreal m_deltaX;
    qreal m_deltaY;
    QRectF m_diagramRect;
    qreal m_diagramHeight;
    qreal m_diagramFunctionRange;
    int m_squareNoMultiplier;
    int m_squareNo;
    QPen m_pen;



    bool m_isMoving;
    QPoint m_mouseStartPoint;
    QPoint m_mouseRelocation;
    QRubberBand *m_rubberBand;


    QPointF *m_current_array;
    QPointF *m_result_array;
    unsigned int m_accuracy;
    unsigned int m_array_lenght;
    unsigned int m_n;

    typedef struct SZoomStackEntry
    {
        qreal deltaX;
        qreal deltaY;
        qreal width;
        qreal height;
        qreal zoomFactor;
    }ZoomStackEntry;

    ZoomStackEntry m_zoomStackEntry;
    QStack<ZoomStackEntry> m_zoomStack;

    QTimer *m_timer;
    int m_textAlpha;

    bool m_isNavigatePointDraw;
    bool m_isPeaksDrawn;
    bool m_isPeakTextEnable;
    bool m_isShowCoordinateLines;
    qreal m_navigatePointX;
private:
    /*Draw functions*/
    void drawBackgroundGrid(QPainter *painter,int squareNo);
    void drawCoordinateLines(QPainter *painter);
    void drawDiagram(QPainter *painter,unsigned int n);
    void drawZoomMSG(QPainter *painter);
    void drawPointOnDiagram(QPainter *painter);
    void drawDiagramPeaks(QPainter *painter);

private:
   void paintEvent(QPaintEvent *);

   /*Mouse Action*/
   void mousePressEvent(QMouseEvent *event);
   void mouseMoveEvent(QMouseEvent *event);
   void mouseReleaseEvent(QMouseEvent *event);
   void wheelEvent( QWheelEvent * event);

   /*Provide the diagram data for plotter*/
   void makeDiagramData(unsigned int n);

public:
   void setDimention(qreal width,qreal height);
   void setGridBGSquareNo(int multiplier);
   void setDiagramAccuracy(unsigned int accuracy);
   void setPointShape(unsigned int pointShape);
   void setDiagramLevel(unsigned int n);
   void setDiagramCoordinateColor(QColor color);
   void setDiagramColor(QColor color);
   void setGridLinesColor(QColor color);
   void setPeaksColor(QColor color);
   void setNavPointColor(QColor color);
   void setLineWidth(qreal lineWidth);
   void setNavPointShow(bool isShown);
   void setPeakShape(unsigned int pointShape);
   void setPeaksShownStatus(bool isPeakShown);
   void setIsShowCoordinateLines(bool isShowCoordinateLines);
   void setIsPeakTextEnable(bool isPeakTextEnable);
   bool saveDiagramOnPath(QString path);
   void restoreDiagram();

public:
    explicit Plotter(QWidget *parent = 0);
    ~Plotter();




signals:
public slots:

private slots:
   void updateTXTCaption();



};

#endif // PLOTTER_H

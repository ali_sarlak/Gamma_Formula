#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMouseEvent>
#include "plotter.h"


namespace Ui {
class MainWindow;
}



typedef enum EGRID_STATUS
{
    EGRID_STATUS_NONE = 0,
    EGRID_STATUS_SET_1 = 1,
    EGRID_STATUS_SET_2 = 2,
    EGRID_STATUS_SET_3 = 3,
    EGRID_STATUS_SET_4 = 4,
    EGRID_STATUS_SET_5 = 5
}GRID_STATUS;


/**
 * @brief The MainWindow class
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT

private:
    QColor m_color_coordinateLines;
    QColor m_color_navPoint;
    QColor m_color_peaks;
    QColor m_color_diagram;
    QColor m_color_grid;

private:

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

private slots:
    void slot_ComboBox_GridBackground(int index);
    void slot_ComboBox_SetLineWidth(int index);
    void slot_ComboBox_SetDiagramLevel(int index);
    void slot_ComboBox_SetPointShape(int index);
    void slot_ComboBox_SetPeakShape(int index);
    void slot_ComboBox_SetDiagramAccuracy(int index);

    void slot_ColorChooser_CoordinateDiagram();
    void slot_ColorChooser_Diagram();
    void slot_ColorChooser_GridLines();
    void slot_ColorChooser_PeaksShape();
    void slot_ColorChooser_NavPoint();

    void slot_CheckBox_SetDiagramCoordinate(bool status);
    void slot_CheckBox_Navigation(bool status);
    void slot_CheckBox_ShowPeaks(bool status);
    void slot_CheckBox_PeaksValues(bool status);

    void slot_Button_Restore();
    void slot_Button_Save();
    void slot_Button_About();
    void slot_Button_Help();




};

#endif // MAINWINDOW_H
